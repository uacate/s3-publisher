<?php

use PHPUnit\Framework\TestCase;
use Uagc\S3Publisher;

class S3PublisherTest extends TestCase
{
    public function testConstructorFailsWithTooFewArgs()
    {
        $this->expectException(ArgumentCountError::class);
        $publisher = new S3Publisher();
    }

    public function testConstructorFailsWithIncorrectParams()
    {
        $params = [
            'foo' => 'bar',
        ];

        $this->expectException(InvalidArgumentException::class);
        $publisher = new S3Publisher($params);
    }

    public function testPublishFailsWithBadSourcePath()
    {
        $aDir = dirname(__FILE__, 2) . "/jibberish";
        $params = [
            "bucket" => getenv('AWS_S3_BUCKET') || "uazgradcoll-testbucket-01",
            "keyPrefix" => "foodir/",
            "preserveSourceDir" => true,
            "region" => "us-west-2"
        ];

        $this->expectException(UnexpectedValueException::class);
        $publisher = new S3Publisher($params);
        $publisher->publish($aDir);
    }

    public function testPublishSucceedsWithValidArgs()
    {
        $aDir = dirname(__FILE__, 2) . "/dir0";
        $params = [
            "bucket" => "uazgradcoll-testbucket-01",
            "keyPrefix" => "foodir/",
            "preserveSourceDir" => true,
            "region" => "us-west-2"
        ];
        $publisher = new S3Publisher($params);
        $published = $publisher->publish($aDir);
        $this->assertTrue($published['successful']);
    }
}
