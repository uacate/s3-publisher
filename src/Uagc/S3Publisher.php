<?php

namespace Uagc;

use Exception;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Aws\S3\S3Client;

class S3Publisher
{
    private $params;

    public function __construct($params)
    {
        $validKeys = ["bucket", "keyPrefix", "preserveSourceDir", "region"];
        $paramKeys = array_keys($params);
        sort($paramKeys);

        // validate parameters
        if ($paramKeys !== $validKeys) {
            throw new \InvalidArgumentException("Improper initialization.");
        }

        $this->params = $params;

        // Set s3 client
        $this->s3Client = new S3Client([
            'region' => $this->params['region'],
            'version' => 'latest'
        ]);
    }

    /**
     * @param $aDir is the dictory containg the source files/dirs.
     */
    public function publish($aDir)
    {
        $published = [
            'successful' => false,
            'message' => '',
            'published' => []
        ];
        $fileList = $this->getFileList($aDir);
        $fileList = $this->removeBasePath($fileList, $aDir);

        if (isset($fileList) && count($fileList) > 0) {
            $awsS3Bucket = $this->params['bucket'];

            foreach ($fileList as $f) {
                $pathInfo = pathinfo($f);

                $fileExt = $this->getFileExt($pathInfo);
                $mimeType = $this->getMimeType($fileExt);

                $awsObject = [
                    'Bucket' => $awsS3Bucket,
                    'Key' => $this->params['keyPrefix'] . $f,
                    'Body' => fopen($aDir . "/" . $f, 'r'),
                    'ContentType' => $mimeType,
                ];

                try {
                    $this->s3Client->putObject($awsObject);
                    $published['successful'] = true;
                    $published['message'] = 'Upload successful';
                    $published['published'][] = $awsObject['Key'];
                } catch (S3Exception $e) {
                    $published['successful'] = false;
                    $published['message'] = 'One or more uploads failed';
                    break;
                }
            }
        }

        return $published;
    }

    private function removeBasePath($fileList, $basePath)
    {
        $flist = [];
        foreach ($fileList as $key => $value) {
            $flist[] = str_replace($basePath . "/", "", $value);
        }
        return $flist;
    }

    private function getMimeType($fileExt)
    {
        $mimeType = "";

        switch ($fileExt) {
            case ".css":
                $mimeType = "text/css";
                break;
            case ".csv":
                $mimeType = "text/csv";
                break;
            case ".html":
                $mimeType = "text/html";
                break;
            case ".js":
                $mimeType = "text/javascript";
                break;
            case ".json":
                $mimeType = "application/json";
                break;
            case ".md":
                $mimeType = "text/markdown";
                break;
            case ".txt":
                $mimeType = "text/plain";
                break;
            case ".yml":
                $mimeType = "text/x-yaml";
                break;
            default:
                if (!is_null($fileExt)) {
                    $mimeType = $fileExt;
                } else {
                    $mimeType = "application/octet-stream";
                }
        }
        return $mimeType;
    }

    private function getFileExt($pathInfo)
    {
        $fileExt = "";
        if (key_exists("extension", $pathInfo)) {
            $fileExt = $pathInfo['extension'];
        }
        return $fileExt;
    }

    private function getFileList($folder)
    {
        $fileList = [];
        $objects = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($folder),
            RecursiveIteratorIterator::SELF_FIRST
        );
        if (isset($objects)) {
            foreach ($objects as $obj) {
                if (is_file($obj->getPathName())) {
                    $fileList[] = $obj->getPathName();
                }
            }
        }
        return $fileList;
    }

    private function getKey($pathInfo, $keyPrefix)
    {
        $key = "";
        $dirName = $pathInfo['dirname'];
        $baseName = $pathInfo['basename'];
        $startPos = strpos($dirName, $keyPrefix) + strlen($keyPrefix);
        $path = substr($dirName, $startPos);
        $key = $keyPrefix . $path . "/" . $baseName;

        return $key;
    }
}
