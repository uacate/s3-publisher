# S3Publisher

S3Publisher for PHP is a utility for deploying static files to AWS S3. Intended primarily for use in CI/CD tasks. A port of [S3Publisher](https://bitbucket.org/uazgraduatecollege/s3publisher) for NodeJS.
